<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class HomePresenter extends BasePresenter
{
    /** @var ContactModel */
    private $contactModel;

    /** @var \Nette\Mail\IMailer @inject */
    public $mailer;

    private $database;

    protected $emailSettings;

    public function __construct(Nette\Database\Context $database, $emailSettings)
    {
        $this->database = $database;
        $this->emailSettings = $emailSettings;
    }

    public function injectContact(\App\Front\Model\ContactModel $contactModel)
    {
        $this->contactModel = $contactModel;
    }

    protected function createComponentContactForm()
    {
        $form = new Form;

        $form->addText('name')->setAttribute('placeholder', 'Jméno')->setAttribute('class', 'input-field')
            ->addRule(Form::FILLED, 'Zadejte jméno')
            ->setRequired(true);

        $form->addText('lastName')->setAttribute('placeholder', 'Příjmení')->setAttribute('class', 'input-field')
            ->addRule(Form::FILLED, 'Zadejte příjmení')
            ->setRequired(true);

        $form->addText('email')->setAttribute('placeholder', 'Zadejte Váš e-mail')->setAttribute('class', 'input-field')
            ->addRule(Form::EMAIL, 'Zadejte e-mail')
            ->setRequired(true);

        $form->addText('date')->setAttribute('placeholder', 'Vložte datum')->setAttribute('class', 'input-field')
            ->setType('date')
            ->addRule(Form::FILLED, 'Zadejte datum')
            ->setRequired(true)
            ->addRule(Form::PATTERN, 'Datum musí být ve formátu dd.mm.rrrr', '^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$');

        $form->addSubmit('send', 'Odeslat')->setAttribute('class', 'button');

        $form->onSuccess[] = [$this, 'processContactForm'];

        return $form;
    }

    public function processContactForm(Form $form)
    {
        $values = $form->getValues(true);

        $date = \DateTime::createFromFormat('d.m.Y', $values['date']);
        $values['date'] = $date->format('Y-m-d');
        $contact = $this->contactModel->saveEmail($values);

        $template = $this->createTemplate();
        $template->setFile(__DIR__ . '/../templates/emails/emailTemplate.latte');
        $template->title = 'Zpráva z kontaktního formuláře';
        $template->values = $values;

        $mail = new Message;
        $mail->setFrom($values['email'])
            ->addTo($this->emailSettings['email'])
            ->setSubject($this->emailSettings['subject'])
            ->setHTMLBody($template);

        $mailer = new SendmailMailer;
        $mailer->send($mail);

        $this->flashMessage('Zpráva byla odeslána');
        $this->redirect('Home:default');
    }
}
