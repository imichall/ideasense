<?php

namespace App\Front\Model;

use Nette;

class ContactModel
{
    use Nette\SmartObject;

    /**
     * @var Nette\Database\Context
     */

    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getContact($contactId)
    {
        return $this->database->table('contact')
            ->where('id', $contactId)
            ->order('created_at DESC');
    }

    public function saveEmail($values)
    {
        return $this->database->table('contact')->insert($values);
    }

}
